import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {SessionStorageService} from '../_services/session-storage.service';
import {mergeMap, catchError} from 'rxjs/operators';

@Injectable()
export class AuthHttpClientInterceptor implements HttpInterceptor {

  constructor(
    private sessionStorageService: SessionStorageService
  ) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    // add the ticket into http request
    // const authReq = req.clone({
    //   headers: req.headers
    //     .set(
    //       'ticket',
    //       this.sessionStorageService.getTicketFromSessionStorage()
    //     )
    //     .set('X-Requested-With', 'XMLHttpRequest')
    // });

    const authReq = req.clone({
      setHeaders: {
        'X-Requested-With': 'XMLHttpRequest',
        ticket: this.sessionStorageService.getTicketFromSessionStorage()
      }
    });


    console.log('---- AuthHttpClientInterceptor runs');
    console.log(`!!!! ${JSON.stringify(authReq)}`);

    return next.handle(authReq);
  }

}
