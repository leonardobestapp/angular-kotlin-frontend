import { Injectable, Inject } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router,
} from '@angular/router';
import {SessionStorageService} from '../_services/session-storage.service';
import {AuthenticationService} from '../_services/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class AuthCanActivate implements CanActivate {

  private readonly casAuthenticateURL = 'http://localhost:8443/cas';

  constructor(
    private router: Router,
    private sessionStorageService: SessionStorageService,
    private authenticationService: AuthenticationService
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot)
    : boolean
  {
    const paramTicket = this.getQueryString('ticket');

    console.log('---- canActivate');

    if (paramTicket) {

      // Here runs after cas authentication is finished
      const ticketValidator = this.authenticationService.ticketValidator(
        paramTicket,
        window.location.origin + window.location.pathname
      );

      // // ticket is valid
      // if (ticketValidator) {
      //   this.sessionStorageService.setTicketToSessionStorage( paramTicket );
      //   // remove ticket from url
      //   // @ts-ignore
      //   window.location.href = route.routeConfig.path;
      // }
      // else {
      //   // ??????? later need to modify after integration front end + back end + cas server
      //
      //   let ticket = this.sessionStorageService.getTicketFromSessionStorage();
      //   if (! ticket) {
      //     ticket = this.authenticationService.getTicket();
      //     if (ticket) {
      //       this.sessionStorageService.setTicketToSessionStorage( ticket );
      //     }
      //   }
      //
      //   if (ticket) {
      //     return true;
      //   }

        // go to cas server to log in
        // window.location.href =
        //   this.casAuthenticateURL + '/login?service=' + window.location.href;
      // }
    }

    console.log('---- canActivate return false');

    return false;
  }

  private getQueryString(name: string): string | null {

    const reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
    const r = window.location.search.substr(1).match(reg);
    if (r) {
      return decodeURI(r[2]);
    }
    return null;
  }

}
