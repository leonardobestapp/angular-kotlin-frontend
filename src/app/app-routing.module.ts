import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HomeComponent} from './_components/home/home.component';
import {AuthCanActivate} from './security/AuthCanActivate';

const routes: Routes = [
  { path: 'home', component: HomeComponent, canActivate: [AuthCanActivate] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
