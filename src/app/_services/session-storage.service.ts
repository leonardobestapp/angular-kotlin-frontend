import { Injectable } from '@angular/core';
import {tick} from '@angular/core/testing';

@Injectable({
  providedIn: 'root'
})
export class SessionStorageService {

  private readonly TICKET = 'ticket';

  constructor() { }

  setTicketToSessionStorage(ticket: string): void {
    sessionStorage.setItem(this.TICKET, ticket);
  }

  getTicketFromSessionStorage(): string {
    return sessionStorage.getItem(this.TICKET) as string;
  }

  deleteTicketFromSessionStorage(): void {
    sessionStorage.removeItem(this.TICKET);
  }
}
