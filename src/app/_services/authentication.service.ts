import { Injectable } from '@angular/core';
import {ajax} from 'rxjs/ajax';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private readonly baseURL = 'http://localhost:8080';
  private readonly getTicketURL = `${this.baseURL}/authenticate/getTicket?1=1`;
  private readonly ticketValidatorURL = `${this.baseURL}/authenticate/ticketValidator?1=1`;

  constructor(
    private httpClient: HttpClient
  ) { }

  getTicket(): string {
    let ticket = '';

    ajax({
      url: this.getTicketURL,
      method: 'GET',
      async: false,
      responseType: 'json'
    }).subscribe(
      res => {
        ticket = res.response as string;
      },
      error => {
        console.error(error);
      }
    );

    return ticket;
  }

  // @ts-ignore
  ticketValidator(ticket: string, service: string): boolean {
    console.log('---- ticketValidator');

    let ticketValidator = false;

    this.httpClient.get<boolean>(this.ticketValidatorURL + '&ticket=' + ticket + '&service=' + service, {})
      .subscribe(
        res => {
          ticketValidator = res;
          return true;
        },
        error => {
          console.error(error);
          return false;
        }
      );

  //   ajax({
  //     url: this.ticketValidatorURL + '&ticket=' + ticket + '&service=' + service,
  //     method: 'GET',
  //     async: false,
  //     responseType: 'JSON'
  //   }).subscribe(
  //     res => {
  //       ticketValidator = res.response as boolean;
  //     },
  //     error => {
  //       console.error(error);
  //     }
  //   );
  //
  //   return ticketValidator;
  }
}
