import {Component, OnInit} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {tick} from '@angular/core/testing';
import {tap} from 'rxjs/operators';
import {Message} from './model/Message';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'kotlin-frontend-exer';

  private readonly baseURL = 'http://localhost:8080';
  private readonly publicListURL = `${this.baseURL}/public/list?1=1`;
  private readonly ticketValidatorURL = `${this.baseURL}/authenticate/ticketValidator?1=1`;

  constructor(
    private httpClient: HttpClient
  ) {
  }

  ngOnInit(): void {

    console.log('---- app runs');
  }

  onPrivate(): void {
    const ticketVar = 'adskfkadfk';
    const service = 'http://localhost:4200';

    const httpOptions = {
      headers: new HttpHeaders({
        observe: 'response',
        'X-Requested-With': 'XMLHttpRequest',
        ticket: ticketVar,
      })
    };

    this.httpClient.get<any>(this.ticketValidatorURL + '&ticket=' + ticketVar + '&service=' + service, httpOptions)
      .pipe(tap(res => {
        console.log(`!! ${res}`);
      }))
      .subscribe(
        res => {
          console.log(`---- ${res}`);
        },
        error => {
          if (error) {
            if (error.status === 401) {
              console.error('---- 401, go to cas server');
            }
          }
          else {
            console.error(`xxxx ${JSON.stringify(error)}`);
          }
        }
      );
  }

  onPublic(): void {
    const ticketVar = 'adskfkadfk';
    const service = 'http://localhost:4200';

    // this.httpClient.get<boolean>(this.ticketValidatorURL + '&ticket=' + ticketVar + '&service=' + service, httpOptions)
    this.httpClient.get<Message[]>(this.publicListURL)
      .subscribe(
        res => {
          console.log(`---- ${res}`);
          if (res) {
            for (let each of res) {
              console.log(`---- ${each.id}, ${each.text}`);
            }
          }

        },
        error => {
          console.error(`xxxx ${JSON.stringify(error)}`);
        }
      );
  }
}
