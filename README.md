# Angular + Kotlin (Spring boot) demo

## How to run
```
$ ng serve --open

Then, check the console of Chrome browser.

Output:
Angular is running in development mode. Call enableProdMode() to enable production mode.
test.component.ts:23 each 1, Hello!
test.component.ts:23 each 2, Bonjour!
test.component.ts:23 each 3, Privet!
test.component.ts:33 each 1, Hello!
test.component.ts:33 each 2, Bonjour!
test.component.ts:33 each 3, Privet!
client:52 [WDS] Live Reloading enabled.
```
